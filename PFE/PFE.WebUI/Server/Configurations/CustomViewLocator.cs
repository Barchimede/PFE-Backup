﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Configurations
{
    public class CustomViewLocator : IViewLocationExpander
    {
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            //Replace folder view with CustomViews
            return viewLocations.Select(f => f.Replace("/Views/", "/Server/Views/"));
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        { }
    }
}
