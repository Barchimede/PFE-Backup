﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.ViewModels
{
    public class ImportModel
    {
        public string Local { get; set; }

        IEnumerable<MachineImport> MachineImports { get; set; }
    }

    public class MachineImport
    {
        public int MachineId { get; set; }

        public string Comment { get; set; }

        public string IpAddress { get; set; }

        public string MacAddress { get; set; }

        public string MachineName { get; set; }
    }
}
