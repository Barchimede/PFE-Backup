﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Models
{
    public partial class Machine
    {
        [Required(ErrorMessage = "The machine id is required Required")]
        public int MachineId { get; set; }

        [Required(ErrorMessage = "The machine name is required")]
        public string MachineName { get; set; }

        [Required(ErrorMessage = "The IP address is required")]
        [StringLength(15, ErrorMessage = "The IP address cannot be longer than 15 characters")]
        public string IpAdress { get; set; }

        [Required(ErrorMessage = "The MAC address is required")]
        [StringLength(39, ErrorMessage = "The MAC address cannot be longer than 39 characters")]
        public string MacAdress { get; set; }

        [Required(ErrorMessage = "The local is required")]
        [StringLength(255, ErrorMessage = "The local cannot be longer than 255 characters.")]
        public string Local { get; set; }

        [Required(ErrorMessage = "The working state is required")]
        public bool Working { get; set; }

        [StringLength(255, ErrorMessage = "The comment cannot be longer than 255 characters")]
        public string Comment { get; set; }

        //Navigation property
        public ICollection<Problem> Problems { get; set; } //I have a collection of them too...
    }
}
