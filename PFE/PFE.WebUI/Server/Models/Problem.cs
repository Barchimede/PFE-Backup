﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Models
{
    public partial class Problem
    {

        [Required(ErrorMessage = "The id of the problem is required")]
        public int ProblemId { get; set; }

        [Required(ErrorMessage = "The machine is required")]
        public int MachineId { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [StringLength(255, ErrorMessage = "The email address cannot be longer than 255 characters")]
        [EmailAddress(ErrorMessage = "The email address is not valid.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The description is required")]
        [StringLength(255, ErrorMessage = "The description cannot be longer than 255 characters")]
        public string Description { get; set; }

        [Required(ErrorMessage = "The picture is required Required")]
        public string Picture { get; set; }

        [Required(ErrorMessage = "The date of the problem is required")]
        public DateTime DateProblem { get; set; }

        //Navigation property
        public Machine Machine { get; set; }

    }
}
