﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Models.Repositories
{
    public class MachineRepository : BaseRepository<Machine>
    {
        public MachineRepository(ApplicationDbContext context) : base(context)
        { }
    }
}
