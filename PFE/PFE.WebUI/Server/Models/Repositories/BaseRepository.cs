﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Models.Repositories
{
    public class BaseRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext _context;

        public BaseRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Delete(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            _context.SaveChanges();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task InsertAsync(TEntity entity)
        {
            await this._context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public void Insert(TEntity entity)
        {
            this._context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
        }

        public bool Save(TEntity entity, Expression<Func<TEntity, bool>> predicate)
        {
            TEntity result = this.SearchFor(predicate).FirstOrDefault();
            if (result == null)
            {
                this.Insert(entity);
                return true;
            }
            return false;
        }

        public async Task<bool> SaveAsync(TEntity entity, Expression<Func<TEntity, bool>> predicate)
        {
            TEntity result = this.SearchFor(predicate).FirstOrDefault();
            if (result == null)
            {
                await InsertAsync(entity);
                return true;
            }
            return false;
        }

        public IQueryable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate)
        {
            return this._context.Set<TEntity>().Where(predicate);
        }
    }
}
