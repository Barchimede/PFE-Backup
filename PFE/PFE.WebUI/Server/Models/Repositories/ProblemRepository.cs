﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Models.Repositories
{
    public class ProblemRepository : BaseRepository<Problem>
    {
        public ProblemRepository(ApplicationDbContext context) : base(context)
        { }
    }
}
