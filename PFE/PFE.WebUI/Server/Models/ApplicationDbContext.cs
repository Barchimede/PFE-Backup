﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PFE.WebUI.Server.Models
{
    public class ApplicationDbContext : DbContext
    {
        public virtual DbSet<Machine> Machines { get; set; }
        public virtual DbSet<Problem> Problems { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Problem>(options =>
            {
                options.HasKey(prob => prob.ProblemId);
                options.HasOne(prob => prob.Machine)
                    .WithMany(machine => machine.Problems)
                    .HasForeignKey(prob => prob.MachineId);
            });

            modelBuilder.Entity<Machine>(options =>
            {
                options.HasAlternateKey(machine => machine.MachineName);
                options.HasKey(machine => machine.MachineId);
                options.Property(machine => machine.Working).HasDefaultValue(true);
            });
        }
    }
}
