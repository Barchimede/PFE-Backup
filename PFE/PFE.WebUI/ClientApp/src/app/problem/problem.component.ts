import { Component, OnInit } from '@angular/core';
import { ProblemService } from '../problem.service';
import { Problem } from '../problem';

@Component({
  selector: 'app-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.css']
})
export class ProblemComponent implements OnInit {

  problems: Problem[];

  constructor(private problemService: ProblemService) { }

  ngOnInit() {
    this.getProblems();
  }
  getProblems(): void {
    this.problemService.getProblem()
        .subscribe(problems => this.problems = problems);
  }

}
  
