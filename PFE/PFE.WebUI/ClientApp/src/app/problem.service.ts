import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Problem } from './problem';
import { PROBLEMS } from './mock-problem';

@Injectable()
export class ProblemService {

  constructor() { }

  getProblem(): Observable<Problem[]> {

    return of(PROBLEMS);
  }
}