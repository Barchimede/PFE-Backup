import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ProblemComponent } from './problem/problem.component';
import { ProblemService } from './problem.service';
import { DataTablesModule } from 'angular-datatables';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { Ng4FilesModule } from 'angular4-files-upload';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ProblemComponent,
    UploadFileComponent
  ],
  imports: [
    BrowserModule,
    Ng4FilesModule
  ],
  providers: [ProblemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
