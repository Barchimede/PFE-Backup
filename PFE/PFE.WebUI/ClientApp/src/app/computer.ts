export class Computer{
    id : number;
    name : string;
    ipAdress : string;
    local : string;
    working : boolean;
    macAdress : string;
    comment : string;
}