import { Problem } from './problem';

export const PROBLEMS: Problem[] = [
  { id: 1, 
    idMachine : 1,
    email : 'gillesrenson@student.vinci.be',
    description : 'Il n y a pas internet',
    dateProblem : '06-02-2018' },
  { id: 2, 
    idMachine : 1,
    email : 'BrigitteLehmann@vinci.be',
    description : 'Il n y a pas internet',
    dateProblem : '05-02-2018' },
  { id: 3, 
    idMachine : 1,
    email : 'gillesrenson@teacher.vinci.be',
    description : 'Il n y a pas internet tout est cassé',
    dateProblem : '06-02-2018' }
];
